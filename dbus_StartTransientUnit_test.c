/*
 * Compile with:
 * gcc $(pkg-config --libs --cflags dbus-1) -o main main.c
 * gcc -ggdb3 -I. $(pkg-config --libs --cflags dbus-1) -o main main2.c dbus-print-message.c
 */
#include <dbus/dbus.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <pthread.h>
#include <signal.h>

#define SLURM_ERROR 1
#define SLURM_SUCCESS 0
#define CGROUP_LIMIT_MAX ((uint64_t) -1)
#define SYSTEMD_MAX_WAIT 2000 //miliseconds

static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
static char *job_path = NULL;

static void _process_signal_reply_msg(DBusMessage *msg)
{
	DBusMessageIter itr;
	int type, tmp_uint;
	char *tmp_str;

	if (!dbus_message_is_signal(msg, "org.freedesktop.systemd1.Manager",
				    "JobRemoved"))
		return;
	/*
	 * The signal for JobRemove should return id, job path, unit and result.
	 *   JobRemoved(u id, o job, s unit, s result)
	 */
	dbus_message_iter_init(msg, &itr);
	do {
		type = dbus_message_iter_get_arg_type(&itr);
		switch (type) {
		case DBUS_TYPE_INVALID:
			printf("Invalid argument type in message\n");
			break;
		case DBUS_TYPE_STRING:
		case DBUS_TYPE_SIGNATURE:
		case DBUS_TYPE_OBJECT_PATH:
			dbus_message_iter_get_basic(&itr, &tmp_str);
			printf("RCVD_STR: %s\n", tmp_str);
			break;
		case DBUS_TYPE_UINT32:
			dbus_message_iter_get_basic(&itr, &tmp_uint);
			printf("RCVD_UINT32: %d\n", tmp_uint);
		default:
			printf("%s: Invalid response type %c not supported by Slurm\n",
			      __func__, type);
			break;
		}
	} while (dbus_message_iter_next(&itr));
}

static int _process_and_close_abandon_reply_msg(DBusMessage *msg)
{
	DBusMessageIter itr;
	int type, rc = SLURM_SUCCESS;
	char *tmp_str;

	dbus_message_iter_init(msg, &itr);
	do {
		type = dbus_message_iter_get_arg_type(&itr);
		switch (type) {
		case DBUS_TYPE_INVALID:
			/* AbandonScope doesn't return anything on success. */
			break;
		case DBUS_TYPE_STRING:
		case DBUS_TYPE_SIGNATURE:
			rc = SLURM_ERROR;
			dbus_message_iter_get_basic(&itr, &tmp_str);
			printf("Got an error an error on dbus AbandonScope: %s\n",
			      tmp_str);
			break;
		default:
			rc = SLURM_ERROR;
			printf("%s: Invalid response type %c not supported by Slurm\n",
			      __func__, type);
			break;
		}
	} while (dbus_message_iter_next(&itr));

	dbus_message_unref(msg);

	if (rc == SLURM_SUCCESS)
		printf("CGROUP, Successfully abandoned scope.\n");

	return rc;
}

static char *_process_and_close_reply_msg(DBusMessage *msg)
{
	DBusMessageIter itr;
	int type, rc = SLURM_SUCCESS;
	char *tmp_str;
	char *job_path = NULL;

	dbus_message_iter_init(msg, &itr);
	do {
		type = dbus_message_iter_get_arg_type(&itr);
		switch (type) {
		case DBUS_TYPE_OBJECT_PATH:
			dbus_message_iter_get_basic(&itr, &tmp_str);
			printf("Possibly created new scope: %s\n", tmp_str);
			job_path = strdup(tmp_str);
			break;
		case DBUS_TYPE_STRING:
		case DBUS_TYPE_SIGNATURE:
			dbus_message_iter_get_basic(&itr, &tmp_str);
			printf("The unit may already exist or we got an error: %s\n",
				 tmp_str);
			break;
		default:
			printf("Invalid response type %c not supported by Slurm\n",
			       type);
			break;
		}
	} while (dbus_message_iter_next(&itr));

	dbus_message_unref(msg);

	return job_path;
}

/*
 * This function waits for a JobRemoved signal in the bus.
 * If the job doesn't exist it just returns.
 */
static void *_dbus_signal_monitor()
{
	DBusConnection *conn = NULL;
	DBusError err;
	DBusMessage* msg;
	int terminate_retries = 10;
	bool terminating = false;

	printf("%s: Starting dbus signal monitor thread.\n", __func__);

	dbus_error_init(&err);
	conn = dbus_bus_get(DBUS_BUS_SYSTEM, &err);

	if (dbus_error_is_set(&err)) {
		printf("Error connecting to dbus system daemon (%s)\n",
		       err.message);
		dbus_error_free(&err);
	}
	if (!conn)
		pthread_exit((void *)-1);

	/*
	 * systemd .Manager interface provides signal JobRemoved (uoss)
	 * Add a rule here for which messages we want to see.
	 * If you pass NULL for the error, this function will not block.
	 * Check dbus function documentation in http://dbus.freedesktop.org.
	 */
	dbus_bus_add_match(conn,
			   "type='signal',"
			   "interface='org.freedesktop.systemd1.Manager',"
			   "path='/org/freedesktop/systemd1',"
			   "member='JobRemoved'",
			   &err);
	dbus_connection_flush(conn);
	if (dbus_error_is_set(&err)) {
		fprintf(stderr, "Match Error (%s)\n", err.message);
		pthread_exit((void *)-1);
	}
	printf("%s: Match rule sent, waiting for JobRemoved dbus signal\n",
	       __func__);

	while(terminate_retries > 0) {
		if (!terminating) {
			pthread_mutex_lock(&lock);
			if (job_path) {
				printf("%s: Thread got the job path as %s\n",
				       __func__, job_path);
				terminating = true;
			}
			pthread_mutex_unlock(&lock);
		} else {
			terminate_retries--;
		}
		/* Wait for our signal being emmitted */
		dbus_connection_read_write(conn, SYSTEMD_MAX_WAIT);
		msg = dbus_connection_pop_message(conn);
		if (!msg)
			break;
		_process_signal_reply_msg(msg);
		dbus_message_unref(msg);
	}
	dbus_connection_unref(conn);
}

static int _abandon_scope(const char *scope_name)
{
	DBusMessage *msg;
	DBusMessageIter args_itr = DBUS_MESSAGE_ITER_INIT_CLOSED;
	DBusConnection *conn = NULL;
	DBusPendingCall *pending;
	DBusError err;

	printf("CGROUP, Abandoning Slurm scope %s\n", scope_name);

	dbus_error_init(&err);
	conn = dbus_bus_get(DBUS_BUS_SYSTEM, &err);

	if (dbus_error_is_set(&err)) {
		printf("%s: cannot connect to dbus system daemon: %s\n",
		      __func__, err.message);
		dbus_error_free(&err);
	}
	if (!conn)
		return SLURM_ERROR;

	msg = dbus_message_new_method_call("org.freedesktop.systemd1",
					   "/org/freedesktop/systemd1",
					   "org.freedesktop.systemd1.Manager",
					   "AbandonScope");
	if (!msg) {
		printf("%s: not enough memory setting dbus msg.\n", __func__);
		return SLURM_ERROR;
	}

	dbus_message_iter_init_append(msg, &args_itr);
	if (!dbus_message_iter_append_basic(&args_itr, DBUS_TYPE_STRING,
					    &scope_name)) {
		printf("%s: memory couldn't be allocated while appending argument.\n",
		      __func__);
		return SLURM_ERROR;
	}
	printf("CGROUP, dbus AbandonScope msg signature: %s\n",
		 dbus_message_get_signature(msg));

	if (!dbus_connection_send_with_reply(conn, msg, &pending, -1)) {
		printf("%s: failed to send dbus message.\n", __func__);
		return SLURM_ERROR;
	}
	if (!pending) {
		printf("%s: could not get a handle for dbus reply.\n", __func__);
		return SLURM_ERROR;
	}

	dbus_connection_flush(conn);
	dbus_message_unref(msg);
	dbus_pending_call_block(pending);
	if (!(msg = dbus_pending_call_steal_reply(pending))) {
		dbus_connection_unref(conn);
		printf("%s: cannot abandon scope, dbus reply msg is null.\n",
		      __func__);
		return SLURM_ERROR;
	}
	dbus_pending_call_unref(pending);
	dbus_connection_unref(conn);

	if (_process_and_close_abandon_reply_msg(msg) != SLURM_SUCCESS)
		return SLURM_ERROR;

	return SLURM_SUCCESS;
}

static bool _set_scope_properties(DBusMessageIter *main_itr, pid_t *p,
				  int npids, bool delegate)
{
	DBusMessageIter it[4] = DBUS_MESSAGE_ITER_INIT_CLOSED;
	const char *pid_prop_name = "PIDs";
	const char *dlg_prop_name = "Delegate";
	const char *tasksmax_prop_name = "TasksMax";
	const char pid_prop_sig[] = { DBUS_TYPE_ARRAY, DBUS_TYPE_UINT32, '\0' };
	const char dlg_prop_sig [] = { DBUS_TYPE_BOOLEAN, '\0' };
	const char tasksmax_prop_sig[] = { DBUS_TYPE_UINT64, '\0' };
	char sig[5];
	int dlg = delegate ? 1 : 0;
	uint64_t tasksmax_val = CGROUP_LIMIT_MAX;

	/* Signature for the container - (sv) part */
	sig[0] = DBUS_STRUCT_BEGIN_CHAR;
	sig[1] = DBUS_TYPE_STRING;
	sig[2] = DBUS_TYPE_VARIANT;
	sig[3] = DBUS_STRUCT_END_CHAR;
	sig[4] = '\0';

	/* Open array - a(sv) */
	if (!dbus_message_iter_open_container(main_itr, DBUS_TYPE_ARRAY, sig,
					      &it[0]))
		goto oom;

	/*
	 * Add PIDs property - PIDs <pid1, pid2, ...>
	 */
	/* Open struct */
	if (!dbus_message_iter_open_container(&it[0], DBUS_TYPE_STRUCT, NULL,
					      &it[1]))
		goto abandon;

	/* Insert string */
	if (!dbus_message_iter_append_basic(&it[1], DBUS_TYPE_STRING,
					    &pid_prop_name))
		goto abandon;

	/* Open variant */
	if (!dbus_message_iter_open_container(&it[1], DBUS_TYPE_VARIANT,
					      pid_prop_sig, &it[2]))
		goto abandon;

	/* Open array of uint32 */
	if (!dbus_message_iter_open_container(&it[2], *(pid_prop_sig),
					      (pid_prop_sig + 1), &it[3]))
		goto abandon;

	/* Insert elements */
	if (!dbus_message_iter_append_fixed_array(&it[3], *(pid_prop_sig + 1),
						  &p, npids))
		goto abandon;

	/*
	 * At this point we have the array of pids inserted, let's close this
	 * block: Close array, Close variant, Close struct.
	 */
	if (!dbus_message_iter_close_container(&it[2], &it[3])
	    || !dbus_message_iter_close_container(&it[1], &it[2])
	    || !dbus_message_iter_close_container(&it[0], &it[1]))
		goto abandon;

	/*
	 * Add the property of Delegate = yes. We are into the array (it1) and
	 * we need to open a new struct (it2) to put the string and the boolean.
	 */
	if (!dbus_message_iter_open_container(&it[0], DBUS_TYPE_STRUCT, NULL,
					      &it[1]))
		goto abandon;
	if (!dbus_message_iter_append_basic(&it[1], DBUS_TYPE_STRING,
					    &dlg_prop_name))
		goto abandon;
	if (!dbus_message_iter_open_container(&it[1], DBUS_TYPE_VARIANT,
					      dlg_prop_sig, &it[2]))
		goto abandon;
	if (!dbus_message_iter_append_basic(&it[2], *(dlg_prop_sig), &dlg))
		goto abandon;

	if (!dbus_message_iter_close_container(&it[1], &it[2])
	    || !dbus_message_iter_close_container(&it[0], &it[1]))
		goto abandon;

	/*
	 * Add the property of TasksMax = infinity. We are into the array (it1)
	 * and we need to open a new struct (it2) to put the string "TasksMax"
	 * and the string "infinity".
	 */
	if (!dbus_message_iter_open_container(&it[0], DBUS_TYPE_STRUCT, NULL,
					      &it[1]))
		goto abandon;
	if (!dbus_message_iter_append_basic(&it[1], DBUS_TYPE_STRING,
					    &tasksmax_prop_name))
		goto abandon;
	if (!dbus_message_iter_open_container(&it[1], DBUS_TYPE_VARIANT,
					      tasksmax_prop_sig, &it[2]))
		goto abandon;
	if (!dbus_message_iter_append_basic(&it[2], *(tasksmax_prop_sig),
					    &tasksmax_val))
		goto abandon;

	if (!dbus_message_iter_close_container(&it[1], &it[2])
	    || !dbus_message_iter_close_container(&it[0], &it[1])
	    || !dbus_message_iter_close_container(main_itr, &it[0]))
		goto abandon;

	return true;

abandon:
	dbus_message_iter_abandon_container_if_open(&it[2], &it[3]);
	dbus_message_iter_abandon_container_if_open(&it[1], &it[2]);
	dbus_message_iter_abandon_container_if_open(&it[0], &it[1]);
	dbus_message_iter_abandon_container_if_open(main_itr, &it[0]);
oom:
	printf("Error: dbus not enough memory, this should not happen!.");
	return false;
}

static bool _set_scope_aux(DBusMessageIter *main_itr)
{
	char sig[9];
	DBusMessageIter it1 = DBUS_MESSAGE_ITER_INIT_CLOSED;

	/*
	 * Systemd's StartTransientUnit method requires to setup this signature
	 * but at the same time requires it to be NULL. This is the last part:
	 * 'a(sa(sv))'
	 */
	sig[0] = DBUS_STRUCT_BEGIN_CHAR;
	sig[1] = DBUS_TYPE_STRING;
	sig[2] = DBUS_TYPE_ARRAY;
	sig[3] = DBUS_STRUCT_BEGIN_CHAR;
	sig[4] = DBUS_TYPE_STRING;
	sig[5] = DBUS_TYPE_VARIANT;
	sig[6] = DBUS_STRUCT_END_CHAR;
	sig[7] = DBUS_STRUCT_END_CHAR;
	sig[8] = '\0';

	/* The array, which will contain the signature but have 0 elements. */
	if (!dbus_message_iter_open_container(main_itr, DBUS_TYPE_ARRAY,
					      sig, &it1))
		goto oom;

	/* Close the array. */
	if (!dbus_message_iter_close_container(main_itr, &it1)) {
		dbus_message_iter_abandon_container_if_open(main_itr, &it1);
		goto oom;
	}

	return true;
oom:
	printf("Error: dbus not enough memory, this should not happen!.");
	return false;
}

/*
 * Slurm function to attach stepd to a systemd scope, using dbus.
 */
static int _stepd_dbus_attach_to_scope(pid_t stepd_pid)
{
	DBusMessage *msg;
	DBusMessageIter args_itr = DBUS_MESSAGE_ITER_INIT_CLOSED;
	DBusConnection *conn = NULL;
	DBusPendingCall *pending;
	DBusError err;
	const char *name = "slurm_dbus.scope";
	const char *mode = "fail";
	pid_t pids[1];
	int npids = 1, rc;
	char *job_path_tmp;
	pthread_t tid;

	printf("Creating Slurm scope %s into system slice and adding pid %d.\n",
	       name, stepd_pid);

	dbus_error_init(&err);

	/*
	 * Connect to the system bus daemon and register our connection.
	 * This function may block until auth. and bus registration are
	 * complete.
	 */
	conn = dbus_bus_get(DBUS_BUS_SYSTEM, &err);

	if (dbus_error_is_set(&err)) {
		printf("Error connecting to dbus system daemon (%s)\n",
		       err.message);
		dbus_error_free(&err);
	}
	if (!conn)
		return SLURM_ERROR;

	/* Create the new method call. */
	msg = dbus_message_new_method_call("org.freedesktop.systemd1",
					   "/org/freedesktop/systemd1",
					   "org.freedesktop.systemd1.Manager",
					   "StartTransientUnit");
	if (!msg) {
		printf("memory couldn't be allocated while creating msg.\n");
		return SLURM_ERROR;
	}

	/*
	 * Initialize the iterator for appending arguments to the end of the
	 * message.
	 */
	dbus_message_iter_init_append(msg, &args_itr);

	/* Append our scope name to the arguments. */
	if (!dbus_message_iter_append_basic(&args_itr, DBUS_TYPE_STRING,
					    &name)) {
		printf("memory couldn't be allocated while appending argument.\n");
		return SLURM_ERROR;
	}

	/*
	 * Append the scope mode. Normally it is 'fail' or 'replace'.
	 * Check systemd docs fore more info.
	 */
	if (!dbus_message_iter_append_basic(&args_itr, DBUS_TYPE_STRING,
					    &mode)) {
		printf("memory couldn't be allocated while appending argument.\n");
		return SLURM_ERROR;
	}

	/*
	 * Start adding specific 'properties' as arguments to our message.
	 * Properties in this context are systemd unit properties. We're
	 * interested in adding Delegate=yes, and the PIDs list (stepd's pid)
	 * which will be moved to this scope container at startup.
	 */
	npids = 1;
	pids[0] = stepd_pid;

	if (!_set_scope_properties(&args_itr, pids, npids, true)) {
		printf("Error setting scope properties, scope not started.");
		return SLURM_ERROR;
	}

	/*
	 * 'Auxiliary units'
	 * Systemd's StartTransientUnit method signature requires to set this
	 * and to be null. These are useless parameters but need to be defined.
	 */
	if (!_set_scope_aux(&args_itr)) {
		printf("Error setting scope aux. units, scope not started.");
		return SLURM_ERROR;
	}


	/* We need to add a rule to receive signals for our job. */
	//slurm_thread_create(&tid, _dbus_signal_monitor, NULL);
	pthread_create(&tid, NULL, _dbus_signal_monitor, NULL);

	printf("CGROUP: msg signature: %s\n", dbus_message_get_signature(msg));

	/*
	 * Queue the msg to send and get a handle for the reply.
	 * -1 is infinite timeout.
	 */
	if (!dbus_connection_send_with_reply(conn, msg, &pending, -1)) {
		printf("Error: failed to send message.");
		return SLURM_ERROR;
       	}

	/* If you want to make a unit to fail, just uncomment this */
	//kill(stepd_pid, SIGKILL);

	if (!pending) {
		printf("Cannot get handle for dbus reply.");
		return SLURM_ERROR;
	}

	/* Block until the outgoing message queue is empty. */
	dbus_connection_flush(conn);

	/* Decrement the ref. count of msg and free it if the cnt is 0. */
	dbus_message_unref(msg);

	/* Wait for the reply. */
	dbus_pending_call_block(pending);
	if (!(msg = dbus_pending_call_steal_reply(pending))) {
		printf("Error starting scope, dbus reply msg is null.\n");
		return SLURM_ERROR;
	}
	dbus_pending_call_unref(pending);

	job_path_tmp = _process_and_close_reply_msg(msg);

	if (!job_path_tmp) {
		dbus_connection_unref(conn);
		return SLURM_ERROR;
	}

	pthread_mutex_lock(&lock);
	job_path = job_path_tmp;
	pthread_mutex_unlock(&lock);

	/* Todo: Check here rc */
	pthread_join(tid, (void *)&rc);

	dbus_connection_unref(conn);

	return _abandon_scope(name);
}

int main(int argc, char** argv)
{
	if (argc != 2) {
		printf("Usage: ./dbus_StartTransientUnit_test <pid>\n");
		return 1;
	}
	return _stepd_dbus_attach_to_scope(atoi(argv[1]));
}
